// src/App.js
import React from "react";
import NavBar from "./components/NavBar";
// New - import the React Router components, and the Profile page component
import Profile from "./components/Profile";
import history from "./utils/history";
import PrivateRoute from "./components/PrivateRoute";
import BeginPIA from "./components/BeginPIA";
import CompletedPIA from "./components/CompletedPIA";
import {
  BrowserRouter as Router,
  Switch
} from "react-router-dom";

function App() {
  return (
    <div className="App">
      {/* Don't forget to include the history module */}
      <Router history={history}>
          <Router />
          <NavBar />

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>

          <PrivateRoute path="/profile" component={Profile}>
            <Profile />
          </PrivateRoute>

          <PrivateRoute path="/beginpia" component={BeginPIA}>
            <BeginPIA />
          </PrivateRoute>

          <PrivateRoute path="/completedpia" component={CompletedPIA}>
            <CompletedPIA />
          </PrivateRoute>

        </Switch>
    </Router>
    </div>
  );
}

export default App;
