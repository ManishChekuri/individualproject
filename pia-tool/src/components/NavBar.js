// src/components/NavBar.js

import React from "react";
import { useAuth0 } from "../react-auth0-spa";
import * as ReactBootStrap from "react-bootstrap"
import { Link } from "react-router-dom";

const NavBar = () => {
  const { isAuthenticated, logout } = useAuth0();

  return (
    <div>

      <ReactBootStrap.Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
      <ReactBootStrap.Navbar.Brand href="#home">Privacy Impact Assessment</ReactBootStrap.Navbar.Brand>
      <ReactBootStrap.Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <ReactBootStrap.Navbar.Collapse id="responsive-navbar-nav">
      <ReactBootStrap.Nav className="mr-auto">

      <span>
        <Link to="/profile">
        <ReactBootStrap.Nav.Link href="#profile">My Profile</ReactBootStrap.Nav.Link>
        </Link>
      </span>

      {/* NEW - add a link to the BeginPIA and CompletedPIA pages */}
      {isAuthenticated && (
        <span>
          <Link to="/beginpia">
          <ReactBootStrap.Nav.Link href="#beginpia">Begin PIA</ReactBootStrap.Nav.Link>
          </Link>
        </span>
      )}

      {isAuthenticated && (
        <span>
          <Link to="/completedpia">
          <ReactBootStrap.Nav.Link href="#completedpia">Completed PIA's</ReactBootStrap.Nav.Link>
          </Link>
        </span>
      )}
      </ReactBootStrap.Nav>
      <ReactBootStrap.Nav>
      {isAuthenticated && (<button align="centre" onClick={() => logout()}>Log out</button>)}
      </ReactBootStrap.Nav>
      </ReactBootStrap.Navbar.Collapse>
      </ReactBootStrap.Navbar>

    </div>
  );
};

export default NavBar;
