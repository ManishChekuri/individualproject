// src/components/BeginPIA.js

import React from "react";
import * as Survey from "survey-react"
import "survey-react/survey.css";
import axios from "axios";
import {useAuth0} from "../react-auth0-spa"
import * as SurveyPDF from 'survey-pdf';

const BeginPIA = () => {
  var surveyJSON = {
 title: "Privacy Impact Assessment",
 pages: [
  {
   name: "pta",
   elements: [
    {
     type: "html",
     name: "pia_heading",
     html: "<h3 style=\"text-align: center;\"><span style=\"color: #000000;\">Welcome to the PIA Tool. </span></h3>\n<h3 style=\"text-align: center;\"><span style=\"color: #000000;\">This service will allow you, as a data controller, to demonstrate your compliance with the following key privacy principles: GDPR, Privacy by Design and Fair Information Practices.</span></h3>\n<p>&nbsp;</p>\n<h4><span style=\"color: #333333;\">Instructions</span></h4>\n<p><span style=\"color: #808080;\">This tool is split into 7 sections and a 'Summary' page. Each section contains numerous questions to evaluate your compliance with various privacy principles. If a particular question is not applicable to your assessment, document your reasons why under that criterion and leave the rating unpicked. There are 3 options to answer each question:</span></p>\n<ul>\n<li><span style=\"color: #808080;\">Not Achieved</span>\n<ul>\n<li><span style=\"color: #808080;\">The criterion is not fulfilled or is limited.</span></li>\n</ul>\n</li>\n<li><span style=\"color: #808080;\">Partially Achieved</span>\n<ul>\n<li><span style=\"color: #808080;\">The criterion is partially fulfilled.</span></li>\n</ul>\n</li>\n<li><span style=\"color: #808080;\">Achieved</span>\n<ul>\n<li><span style=\"color: #808080;\">The criterion is fully realised, and no significant weaknesses can be detected.</span></li>\n</ul>\n</li>\n</ul>\n<p><span style=\"color: #808080;\">After selecting an option, a text box will be available to document the reasons for your choice and to add further evidence. If you opt for the 'Not Achieved' or 'Partially Achieved' options, you'll also be required to undertake a 'Risk Assessment'.</span></p>\n<p><span style=\"color: #808080;\">The 'Risk Assessment' stage comprises of 4 parts. Firstly, you'll be required to identify any risks posed to data subjects as a result of not fully achieving the criterion. Then, you'll need to outline any planned/existing measures put in place to tackle the risk. Finally, you'll be instructed to rate the likelihood and severity of each risk occurring, according to the following scale:</span></p>\n<ul>\n<li><span style=\"color: #808080;\">1 - Undefined</span></li>\n<li><span style=\"color: #808080;\">2 - Negligible</span></li>\n<li><span style=\"color: #808080;\">3 - Limited</span></li>\n<li><span style=\"color: #808080;\">4 - Important</span></li>\n<li><span style=\"color: #808080;\">5 - Maximum</span></li>\n</ul>\n<p><span style=\"color: #808080;\">For each risk identified, the system with then calculate your level of risk (%). The higher the percentage the greater the impact of the risk. Therfore, allowing you to prioritise and delegate the implementation of the measures for controlling the risk.</span></p>\n<p><span style=\"color: #808080;\">Your responses to the questions in each section are weighted and an overall percentage of your compliance in that section is presented - this is your risk measure. A mean average of your overall risk measure is calculated and visible in the final 'Summary' page. This should give you a good idea of your overall compliance level and whether or not you are at risk of failing to comply with regulations.</span></p>\n<p><span style=\"color: #808080;\">Finally, once you have completed the PIA process you'll be presented with a PDF copy of your responses. This document will be downloaded automatically once you have finished. It is very important that you make no changes to the PDF document. Any changes that you do make will not be reflected in the calculations for your risk measures.&nbsp;</span></p>\n<p>&nbsp;</p>\n<h4 style=\"text-align: center;\"><em><span style=\"color: #000000;\">Good Luck with all your privacy endeavours!</span></em></h4>"
    },
    {
     type: "panel",
     name: "pta_panel",
     elements: [
      {
       type: "html",
       name: "pta_heading",
       html: "<h3 style=\"text-align: left;\"><span style=\"color: #000000;\">Section 0: Privacy Threshold Assessment</span></h3>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">Firstly, do you even need to conduct a PIA?</span></p>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">The following 'Yes' or 'No' questions will enable you to determine whether your processing activity is likely to result in high risks to the rights and freedoms of data subjects.</span></p>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">By answering 'Yes' to any of these questions will allow you to begin the PIA process.</span></p>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">If you are even slightly unsure of your answer, opt for the 'Yes' option - better safe than sorry!</span></p>"
      },
      {
       type: "panel",
       name: "pta_questions_panel",
       elements: [
        {
         type: "boolean",
         name: "pta_q1",
         title: "Will the project involve the collection of new information about individuals?",
         description: "Re-use of data collected for one purpose e.g. providing a service but now being used for research is covered by question 4.",
         label: "Will the project involve the collection of new information about individuals?",
         labelTrue: "Yes",
         labelFalse: "No"
        },
        {
         type: "boolean",
         name: "pta_q2",
         title: "Will the project compel individuals to provide information about themselves?",
         description: "This could occur if an organisation has commissioned a research project relating to staff.",
         label: "Will the project compel individuals to provide information about themselves?",
         labelTrue: "Yes",
         labelFalse: "No"
        },
        {
         type: "boolean",
         name: "pta_q3",
         title: "Will information about individuals be disclosed to organisations or people who have not previously had routine access to the information? ",
         description: "This could also cover situations where an organisation is providing you with information for a research project that they haven’t supplied to a third party before.",
         label: "Will information about individuals be disclosed to organisations or people who have not previously had routine access to the information? ",
         labelTrue: "Yes",
         labelFalse: "No"
        },
        {
         type: "boolean",
         name: "pta_q4",
         title: "Are you using information about individuals for a purpose it is not currently used for, or in a way it is not currently used? ",
         description: "If you are re-using a research data set, then this question won’t apply.",
         label: "Are you using information about individuals for a purpose it is not currently used for, or in a way it is not currently used? ",
         labelTrue: "Yes",
         labelFalse: "No"
        },
        {
         type: "boolean",
         name: "pta_q5",
         title: "Does the project involve you using new technology that might be perceived as being privacy intrusive? For example, the use of biometrics or facial recognition.",
         description: "This would cover things like fingerprint technologies.",
         label: "Does the project involve you using new technology that might be perceived as being privacy intrusive? For example, the use of biometrics or facial recognition.",
         labelTrue: "Yes",
         labelFalse: "No"
        },
        {
         type: "boolean",
         name: "pta_q6",
         title: "Will the project result in you making decisions or taking action against individuals in ways that can have a significant impact on them?",
         description: "If you are conducting research for an organisation that could affect their clients or staff, this may apply.",
         label: "Will the project result in you making decisions or taking action against individuals in ways that can have a significant impact on them?",
         labelTrue: "Yes",
         labelFalse: "No"
        },
        {
         type: "boolean",
         name: "pta_q7",
         title: "Is the information about individuals of a kind particularly likely to raise privacy concerns or expectations? For example, health records, criminal records or other information that people would consider to be private.",
         description: "Or any of the sensitive personal data, that is, ethnicity or racial origin, political beliefs, religious beliefs, trade union membership, sexual life.",
         label: "Is the information about individuals of a kind particularly likely to raise privacy concerns or expectations? For example, health records, criminal records or other information that people would consider to be private.",
         labelTrue: "Yes",
         labelFalse: "No"
        },
        {
         type: "boolean",
         name: "pta_q8",
         title: "Will the project require you to contact individuals in ways that they may find intrusive (for example, marketing calls or emails)?",
         description: "This may vary from individual to individual e.g. some people are happy for their health records to be used for research, others only want them used for their health care.",
         label: "Will the project require you to contact individuals in ways that they may find intrusive (for example, marketing calls or emails)?",
         labelTrue: "Yes",
         labelFalse: "No"
        }
       ]
      }
     ]
    },
    {
     type: "html",
     name: "credits",
     html: "<blockquote>\n<p style=\"text-align: center;\"><span style=\"color: #999999;\"><em>Developed by&nbsp;<a href=\"https://www.linkedin.com/in/manish-chekuri/\">Manish Chekuri</a> during his final year at The University of Glasgow, studying for a BSc (Hons) Computing Science degree. </em></span></p>\n<p style=\"text-align: center;\"><span style=\"color: #999999;\"><em>Supervised by <a href=\"http://www.dcs.gla.ac.uk/~inah/\">Dr. Inah Omoronyia</a>,&nbsp;Lecturer in Software Engineering and Information Security at The University of Glasgow.</em></span></p>\n</blockquote>"
    }
   ]
  },
  {
   name: "section1",
   elements: [
    {
     type: "html",
     name: "s1_heading",
     html: "<h3 style=\"text-align: left;\"><span style=\"color: #000000;\">Section 1: Proactive not Reactive; Preventative not Remedial</span></h3>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">The Privacy by Design approach is characterised by proactive rather than reactive measures. It anticipates and prevents privacy invasive events before they happen. PbD does not wait for privacy risks to materialise, nor does it offer remedies for resolving privacy infractions once they have occurred &minus; it aims to prevent them from occurring. In short, Privacy by Design comes before-the-fact, not after.</span></p>"
    },
    {
     type: "panel",
     name: "s1_panel",
     elements: [
      {
       type: "panel",
       name: "s1_q1_panel",
       elements: [
        {
         type: "rating",
         name: "s1_q1_rating",
         title: "You have explicitly recognised  the value and benefits of proactively adopting strong privacy practices, early and consistently (e.g. preventing (internal) data breaches from happening in the first place).",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s1_q1_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s1_q1_risk_assessment",
         visibleIf: "{s1_q1_rating} = \"10\" or {s1_q1_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s1_q2_panel",
       elements: [
        {
         type: "rating",
         name: "s1_q2_rating",
         title: "You have a clear commitment to set and enforce high standards of privacy.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s1_q2_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s1_q2_risk_assessment",
         visibleIf: "{s1_q2_rating} = \"10\" or {s1_q2_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         defaultValue: [
          {
           "Level of Risk": 0
          }
         ],
         correctAnswer: [
          {
           "Level of Risk": 0
          }
         ],
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         defaultRowValue: {
          "Level of Risk": 0
         },
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s1_q3_panel",
       elements: [
        {
         type: "rating",
         name: "s1_q3_rating",
         title: "You have incorporated a culture of continuous improvement demonstrating that privacy commitments are shared throughout by user communities and stakeholders.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s1_q3_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s1_q3_risk_assessment",
         visibleIf: "{s1_q3_rating} = \"10\" or {s1_q3_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s1_q4_panel",
       elements: [
        {
         type: "rating",
         name: "s1_q4_rating",
         title: "You have established methods to recognise and anticipate poor privacy: designs, practices and outcomes.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s1_q4_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s1_q4_risk_assessment",
         visibleIf: "{s1_q4_rating} = \"10\" or {s1_q4_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s1_q5_panel",
       elements: [
        {
         type: "rating",
         name: "s1_q5_rating",
         title: "You have also established methods to correct any negative impacts, well before they occur in proactive, systematic and innovative ways.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s1_q5_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s1_q5_risk_assessment",
         visibleIf: "{s1_q5_rating} = \"10\" or {s1_q5_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s1_q6_panel",
       elements: [
        {
         type: "rating",
         name: "s1_q6_rating",
         title: "You keep an inventory of systems and where personal data is located within the network architecture.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s1_q6_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s1_q6_risk_assessment",
         visibleIf: "{s1_q6_rating} = \"10\" or {s1_q6_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      }
     ]
    },
    {
     type: "expression",
     name: "s1_risk_measure",
     title: "Risk Measure for Section 1:",
     expression: "(({s1_q1_rating} + {s1_q2_rating} + {s1_q3_rating} + {s1_q4_rating} + {s1_q5_rating} + {s1_q6_rating}) / 60)",
     displayStyle: "percent",
     commentText: "Other (describe)"
    }
   ],
   visibleIf: "{pta_q1} = true or {pta_q2} = true or {pta_q3} = true or {pta_q4} = true or {pta_q5} = true or {pta_q6} = true or {pta_q7} = true or {pta_q8} = true"
  },
  {
   name: "section2",
   elements: [
    {
     type: "html",
     name: "s2_heading",
     html: "<h3 style=\"text-align: left;\"><span style=\"color: #000000;\">Section 2: Privacy as the Default</span></h3>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">We can all be certain of one thing − the default rules! Privacy by Design seeks to deliver the maximum degree of privacy by ensuring that personal data are automatically protected in any given IT system or business practice. If an individual does nothing, their privacy still remains intact. No action is required on the part of the individual to protect their privacy − it is built into the system, by default.</span></p>"
    },
    {
     type: "panel",
     name: "s2_panel",
     elements: [
      {
       type: "panel",
       name: "s2_q1_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q1_rating",
         title: "Before collecting information, you have clearly communicated to the data subject the purposes for which personal information is: collected, used, retained and disclosed.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q1_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q1_risk_assessment",
         visibleIf: "{s2_q1_rating} = \"10\" or {s2_q1_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q2_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q2_rating",
         title: "You have specified purposes that are clear, limited and relevant to the circumstances.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q2_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q2_risk_assessment",
         visibleIf: "{s2_q2_rating} = \"10\" or {s2_q2_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q3_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q3_rating",
         title: "You have defined the intended uses for personal data and established procedures for its processing.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q3_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q3_risk_assessment",
         visibleIf: "{s2_q3_rating} = \"10\" or {s2_q3_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q4_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q4_rating",
         title: "You collect personal information for fair and lawful reasons and limit collection to what is necessary for the specified purposes.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q4_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q4_risk_assessment",
         visibleIf: "{s2_q4_rating} = \"10\" or {s2_q4_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q5_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q5_rating",
         title: "You ensure that the collection of personal identifiable information is kept to a strict minimum.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q5_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q5_risk_assessment",
         visibleIf: "{s2_q5_rating} = \"10\" or {s2_q5_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q6_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q6_rating",
         title: "You have chosen the types of personal data that will be collected and identified a basis for the collection.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q6_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q6_risk_assessment",
         visibleIf: "{s2_q6_rating} = \"10\" or {s2_q6_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q7_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q7_rating",
         title: "You begin with non-identifiable interactions and transactions, as the default, when in the design phase of development.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q7_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q7_risk_assessment",
         visibleIf: "{s2_q7_rating} = \"10\" or {s2_q7_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q8_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q8_rating",
         title: "You minimise the identifiability, observability and link-ability of personal information.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q8_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q8_risk_assessment",
         visibleIf: "{s2_q8_rating} = \"10\" or {s2_q8_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q9_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q9_rating",
         title: "You have obtained consent by the data subject for the use, retention and disclosure of personal information, except where otherwise required by law, limited to the relevant purposes identified.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q9_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q9_risk_assessment",
         visibleIf: "{s2_q9_rating} = \"10\" or {s2_q9_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q10_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q10_rating",
         title: "You have identified retention period(s) for the collected personal data, for as long as necessary, to fulfil the stated purposes and then delete or sanitise the data securely.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q10_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q10_risk_assessment",
         visibleIf: "{s2_q10_rating} = \"10\" or {s2_q10_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q11_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q11_rating",
         title: "You have made sure that the backups are also deleted, after the personal data is deleted.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q11_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q11_risk_assessment",
         visibleIf: "{s2_q11_rating} = \"10\" or {s2_q11_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s2_q12_panel",
       elements: [
        {
         type: "rating",
         name: "s2_q12_rating",
         title: "You employ the most privacy protective setting, as default, when the need or use of personal information is not clear.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s2_q12_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s2_q12_risk_assessment",
         visibleIf: "{s2_q12_rating} = \"10\" or {s2_q12_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      }
     ]
    },
    {
     type: "expression",
     name: "s2_risk_measure",
     title: "Risk Measure for Section 2:",
     expression: "(({s2_q1_rating} + {s2_q2_rating} + {s2_q3_rating} + {s2_q4_rating} + {s2_q5_rating} + {s2_q6_rating} + {s2_q7_rating} + {s2_q8_rating} + {s2_q9_rating} + {s2_q10_rating} + {s2_q11_rating} + {s2_q12_rating}) / 120)",
     displayStyle: "percent",
     commentText: "Other (describe)"
    }
   ],
   visibleIf: "{pta_q1} = true or {pta_q2} = true or {pta_q3} = true or {pta_q4} = true or {pta_q5} = true or {pta_q6} = true or {pta_q7} = true or {pta_q8} = true"
  },
  {
   name: "section3",
   elements: [
    {
     type: "html",
     name: "s3_heading",
     html: "<h3 style=\"text-align: left;\"><span style=\"color: #000000;\">Section 3: Privacy Embedded into Design</span></h3>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">Privacy by Design is embedded into the design and architecture of IT systems and business practices. It is not bolted on as an add-on, after the fact. The result is that privacy becomes an essential component of the core functionality being delivered. Privacy is integral to the system, without diminishing functionality.</span></p>"
    },
    {
     type: "panel",
     name: "s3_panel",
     elements: [
      {
       type: "panel",
       name: "s3_q1_panel",
       elements: [
        {
         type: "rating",
         name: "s3_q1_rating",
         title: "You always consider additional, broader contexts when embedding privacy into technologies, operations, and information architectures.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s3_q1_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s3_q1_risk_assessment",
         visibleIf: "{s3_q1_rating} = \"10\" or {s3_q1_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s3_q2_panel",
       elements: [
        {
         type: "rating",
         name: "s3_q2_rating",
         title: "You consult all stakeholders and interests when embedding privacy into technologies, operations, and information architectures.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s3_q2_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s3_q2_risk_assessment",
         visibleIf: "{s3_q2_rating} = \"10\" or {s3_q2_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s3_q3_panel",
       elements: [
        {
         type: "rating",
         name: "s3_q3_rating",
         title: "You re-invent existing choices, due to alternatives being unacceptable, when embedding privacy into technologies, operations, and information architectures.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s3_q3_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s3_q3_risk_assessment",
         visibleIf: "{s3_q3_rating} = \"10\" or {s3_q3_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s3_q4_panel",
       elements: [
        {
         type: "rating",
         name: "s3_q4_rating",
         title: "You adopt a systemic, principled approach to embedding privacy.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s3_q4_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s3_q4_risk_assessment",
         visibleIf: "{s3_q4_rating} = \"10\" or {s3_q4_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s3_q5_panel",
       elements: [
        {
         type: "rating",
         name: "s3_q5_rating",
         title: "You incorporate accepted standards and frameworks, which are amenable to external reviews and audits.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s3_q5_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s3_q5_risk_assessment",
         visibleIf: "{s3_q5_rating} = \"10\" or {s3_q5_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s3_q6_panel",
       elements: [
        {
         type: "rating",
         name: "s3_q6_rating",
         title: "You conduct and publish detailed, periodic Data Protection Impact Assessments, clearly documenting risks and measures to mitigate.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s3_q6_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s3_q6_risk_assessment",
         visibleIf: "{s3_q6_rating} = \"10\" or {s3_q6_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s3_q7_panel",
       elements: [
        {
         type: "rating",
         name: "s3_q7_rating",
         title: "You demonstrate the minimisation of the privacy impacts of the resulting technology, operation or information architecture and their uses, ensuring that they are not easily degraded through use, misconfiguration or error.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s3_q7_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s3_q7_risk_assessment",
         visibleIf: "{s3_q7_rating} = \"10\" or {s3_q7_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      }
     ]
    },
    {
     type: "expression",
     name: "s3_risk_measure",
     title: "Risk Measure for Section 3:",
     expression: "(({s3_q1_rating} + {s3_q2_rating} + {s3_q3_rating} + {s3_q4_rating} + {s3_q5_rating} + {s3_q6_rating} + {s3_q7_rating}) / 70)",
     displayStyle: "percent",
     commentText: "Other (describe)"
    }
   ],
   visibleIf: "{pta_q1} = true or {pta_q2} = true or {pta_q3} = true or {pta_q4} = true or {pta_q5} = true or {pta_q6} = true or {pta_q7} = true or {pta_q8} = true"
  },
  {
   name: "section4",
   elements: [
    {
     type: "html",
     name: "s4_heading",
     html: "<h3 style=\"text-align: left;\"><span style=\"color: #000000;\">Section 4: Full Functionality – Positive-Sum, not Zero-Sum</span></h3>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">Privacy by Design seeks to accommodate all legitimate interests and objectives in a positive-sum “win- win” manner, not through a dated, zero-sum approach, where unnecessary trade-offs are made. Privacy by Design avoids the pretence of false dichotomies, such as privacy vs. security, demonstrating that it is possible, and far more desirable, to have both.</span></p>"
    },
    {
     type: "panel",
     name: "s4_panel",
     elements: [
      {
       type: "panel",
       name: "s4_q1_panel",
       elements: [
        {
         type: "rating",
         name: "s4_q1_rating",
         title: "You do not impair full functionality when embedding privacy into a given technology, process, or system and satisfy all legitimate objectives − not only the privacy goals.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s4_q1_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s4_q1_risk_assessment",
         visibleIf: "{s4_q1_rating} = \"10\" or {s4_q1_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s4_q2_panel",
       elements: [
        {
         type: "rating",
         name: "s4_q2_rating",
         title: "You optimise all requirements, to the greatest extent possible, when embedding privacy.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s4_q2_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s4_q2_risk_assessment",
         visibleIf: "{s4_q2_rating} = \"10\" or {s4_q2_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s4_q3_panel",
       elements: [
        {
         type: "rating",
         name: "s4_q3_rating",
         title: "You embrace legitimate non-privacy objectives and accommodate them, without making trade-offs to meet privacy goals.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s4_q3_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s4_q3_risk_assessment",
         visibleIf: "{s4_q3_rating} = \"10\" or {s4_q3_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s4_q4_panel",
       elements: [
        {
         type: "rating",
         name: "s4_q4_rating",
         title: "You clearly document all interests and objectives, with desired functions articulated and metrics agreed upon and applied.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s4_q4_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s4_q4_risk_assessment",
         visibleIf: "{s4_q4_rating} = \"10\" or {s4_q4_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s4_q5_panel",
       elements: [
        {
         type: "rating",
         name: "s4_q5_rating",
         title: "You reject any trade-offs as being unnecessary and find a solution that enables multi-functionality.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s4_q5_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s4_q5_risk_assessment",
         visibleIf: "{s4_q5_rating} = \"10\" or {s4_q5_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      }
     ]
    },
    {
     type: "expression",
     name: "s4_risk_measure",
     title: "Risk Measure for Section 4:",
     expression: "(({s4_q1_rating} + {s4_q2_rating} + {s4_q3_rating} + {s4_q4_rating} + {s4_q5_rating}) / 50)",
     displayStyle: "percent",
     commentText: "Other (describe)"
    }
   ],
   visibleIf: "{pta_q1} = true or {pta_q2} = true or {pta_q3} = true or {pta_q4} = true or {pta_q5} = true or {pta_q6} = true or {pta_q7} = true or {pta_q8} = true"
  },
  {
   name: "section5",
   elements: [
    {
     type: "html",
     name: "s5_heading",
     html: "<h3 style=\"text-align: left;\"><span style=\"color: #000000;\">Section 5: End-to-End Security – Lifecycle Protection</span></h3>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">Privacy by Design, having been embedded into the system prior to the first element of information being collected, extends securely throughout the entire lifecycle of the data involved — strong security measures are essential to privacy, from start to finish. This ensures that all data are securely retained, and then securely destroyed at the end of the process, in a timely fashion. Thus, Privacy by Design ensures cradle to grave, secure lifecycle management of information, end-to-end.</span></p>"
    },
    {
     type: "panel",
     name: "s5_panel",
     elements: [
      {
       type: "panel",
       name: "s5_q1_panel",
       elements: [
        {
         type: "rating",
         name: "s5_q1_rating",
         title: "You have defined an approach for data lifecycle management.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s5_q1_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s5_q1_risk_assessment",
         visibleIf: "{s5_q1_rating} = \"10\" or {s5_q1_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s5_q2_panel",
       elements: [
        {
         type: "rating",
         name: "s5_q2_rating",
         title: "You assume responsibility for the security and sensitivity of personal information throughout the lifecycle of the data in question and across the entire domain.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s5_q2_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s5_q2_risk_assessment",
         visibleIf: "{s5_q2_rating} = \"10\" or {s5_q2_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s5_q3_panel",
       elements: [
        {
         type: "rating",
         name: "s5_q3_rating",
         title: "You ensure consistency of personal information with standards that have developed by recognised standards development bodies.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s5_q3_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s5_q3_risk_assessment",
         visibleIf: "{s5_q3_rating} = \"10\" or {s5_q3_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s5_q4_panel",
       elements: [
        {
         type: "rating",
         name: "s5_q4_rating",
         title: "You assure the confidentiality, integrity and availability of personal data throughout its lifecycle by including methods for: secure destruction, appropriate encryption, strong access control to access personal data and monitoring access logs.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s5_q4_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s5_q4_risk_assessment",
         visibleIf: "{s5_q4_rating} = \"10\" or {s5_q4_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s5_q5_panel",
       elements: [
        {
         type: "rating",
         name: "s5_q5_rating",
         title: "You have secure connections in accessing personal data.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s5_q5_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s5_q5_risk_assessment",
         visibleIf: "{s5_q5_rating} = \"10\" or {s5_q5_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s5_q6_panel",
       elements: [
        {
         type: "rating",
         name: "s5_q6_rating",
         title: "You have encrypted the stored personal data.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s5_q6_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s5_q6_risk_assessment",
         visibleIf: "{s5_q6_rating} = \"10\" or {s5_q6_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s5_q7_panel",
       elements: [
        {
         type: "rating",
         name: "s5_q7_rating",
         title: "You have a plan for informing about data breaches.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s5_q7_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s5_q7_risk_assessment",
         visibleIf: "{s5_q7_rating} = \"10\" or {s5_q7_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s5_q8_panel",
       elements: [
        {
         type: "rating",
         name: "s5_q8_rating",
         title: "You have defined a security policy to protect personal data.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s5_q8_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s5_q8_risk_assessment",
         visibleIf: "{s5_q8_rating} = \"10\" or {s5_q8_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      }
     ]
    },
    {
     type: "expression",
     name: "s5_risk_measure",
     title: "Risk Measure for Section 5:",
     expression: "(({s5_q1_rating} + {s5_q2_rating} + {s5_q3_rating} + {s5_q4_rating} + {s5_q5_rating} + {s5_q6_rating} + {s5_q7_rating} + {s5_q8_rating}) / 80)",
     displayStyle: "percent",
     commentText: "Other (describe)"
    }
   ],
   visibleIf: "{pta_q1} = true or {pta_q2} = true or {pta_q3} = true or {pta_q4} = true or {pta_q5} = true or {pta_q6} = true or {pta_q7} = true or {pta_q8} = true"
  },
  {
   name: "section6",
   elements: [
    {
     type: "html",
     name: "s6_heading",
     html: "<h3 style=\"text-align: left;\"><span style=\"color: #000000;\">Section 6: Visibility and Transparency</span></h3>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">Privacy by Design seeks to assure all stakeholders that whatever the business practice or technology involved, it is in fact, operating according to the stated promises and objectives, subject to independent verification. Its component parts and operations remain visible and transparent, to both users and providers alike. Remember, trust but verify!</span></p>"
    },
    {
     type: "panel",
     name: "s6_panel",
     elements: [
      {
       type: "panel",
       name: "s6_q1_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q1_rating",
         title: "You document and communicate responsibility for all privacy-related policies/procedures and assign them to a specific individual (e.g. Data Protection Officer), specifying their duties and rights.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q1_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q1_risk_assessment",
         visibleIf: "{s6_q1_rating} = \"10\" or {s6_q1_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q2_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q2_rating",
         title: "You secure privacy protection through contractual safeguards with the employees involved in processing, when transferring personal information to third parties.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q2_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q2_risk_assessment",
         visibleIf: "{s6_q2_rating} = \"10\" or {s6_q2_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q3_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q3_rating",
         title: "You have organised personal data processing by using data protection agreements or annexes.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q3_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q3_risk_assessment",
         visibleIf: "{s6_q3_rating} = \"10\" or {s6_q3_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q4_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q4_rating",
         title: "You have maintained records of processing activities and descriptions of personal data flows.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q4_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q4_risk_assessment",
         visibleIf: "{s6_q4_rating} = \"10\" or {s6_q4_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q5_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q5_rating",
         title: "You demonstrate accountability by identifying key pieces of data protection documentation.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q5_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q5_risk_assessment",
         visibleIf: "{s6_q5_rating} = \"10\" or {s6_q5_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q6_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q6_rating",
         title: "You make information about the policies and practices relating to the management of personal information readily available to data subjects.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q6_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q6_risk_assessment",
         visibleIf: "{s6_q6_rating} = \"10\" or {s6_q6_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q7_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q7_rating",
         title: "You establish and communicate compliant and redress mechanisms to data subjects, including how to access the next level of appeal.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q7_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q7_risk_assessment",
         visibleIf: "{s6_q7_rating} = \"10\" or {s6_q7_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q8_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q8_rating",
         title: "You take necessary steps to monitor, evaluate and verify compliance with privacy policies and procedures.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q8_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q8_risk_assessment",
         visibleIf: "{s6_q8_rating} = \"10\" or {s6_q8_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q9_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q9_rating",
         title: "You have an easily accessible privacy policy, which is updated regularly, to inform data subjects.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q9_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q9_risk_assessment",
         visibleIf: "{s6_q9_rating} = \"10\" or {s6_q9_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q10_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q10_rating",
         title: "You have provided all necessary information relating to personal data processing for data subjects.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q10_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q10_risk_assessment",
         visibleIf: "{s6_q10_rating} = \"10\" or {s6_q10_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q11_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q11_rating",
         title: "You have translated the privacy policy to relevant EU languages.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q11_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q11_risk_assessment",
         visibleIf: "{s6_q11_rating} = \"10\" or {s6_q11_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s6_q12_panel",
       elements: [
        {
         type: "rating",
         name: "s6_q12_rating",
         title: "You have identified basis/bases for the personal data transfers outside EU or EEA.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s6_q12_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s6_q12_risk_assessment",
         visibleIf: "{s6_q12_rating} = \"10\" or {s6_q12_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      }
     ]
    },
    {
     type: "expression",
     name: "s6_risk_measure",
     title: "Risk Measure for Section 6:",
     expression: "(({s6_q1_rating} + {s6_q2_rating} + {s6_q3_rating} + {s6_q4_rating} + {s6_q5_rating} + {s6_q6_rating} + {s6_q7_rating} + {s6_q8_rating} + {s6_q9_rating} + {s6_q10_rating} + {s6_q11_rating} + {s6_q12_rating}) / 120)",
     displayStyle: "percent",
     commentText: "Other (describe)"
    }
   ],
   visibleIf: "{pta_q1} = true or {pta_q2} = true or {pta_q3} = true or {pta_q4} = true or {pta_q5} = true or {pta_q6} = true or {pta_q7} = true or {pta_q8} = true"
  },
  {
   name: "section7",
   elements: [
    {
     type: "html",
     name: "s7_heading",
     html: "<h3 style=\"text-align: left;\"><span style=\"color: #000000;\">Section 7: Respect for User Privacy</span></h3>\n<p style=\"text-align: left;\"><span style=\"color: #808080;\">Above all, Privacy by Design requires architects and operators to keep the interests of the individual uppermost by offering such measures as strong privacy defaults, appropriate notice, and empowering user-friendly options. Keep it user-centric!</span></p>"
    },
    {
     type: "panel",
     name: "s7_panel",
     elements: [
      {
       type: "panel",
       name: "s7_q1_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q1_rating",
         title: "You consciously design around the interests and needs of individual users.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q1_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q1_risk_assessment",
         visibleIf: "{s7_q1_rating} = \"10\" or {s7_q1_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q2_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q2_rating",
         title: "You empower data subjects to play an active role in the management of their own data, to check against abuses and misuses of privacy and personal data.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q2_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q2_risk_assessment",
         visibleIf: "{s7_q2_rating} = \"10\" or {s7_q2_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q3_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q3_rating",
         title: "You obtain the data subject's free and specific consent for the collection, use or disclosure of personal information, except where otherwise permitted by law.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q3_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q3_risk_assessment",
         visibleIf: "{s7_q3_rating} = \"10\" or {s7_q3_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q4_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q4_rating",
         title: "You ensure that the quality of the consent is more specific and clear for more sensitive data.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q4_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q4_risk_assessment",
         visibleIf: "{s7_q4_rating} = \"10\" or {s7_q4_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q5_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q5_rating",
         title: "You allow data subject's to withdraw consent at a later date and thus erase their personal data, as well as back-ups.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q5_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q5_risk_assessment",
         visibleIf: "{s7_q5_rating} = \"10\" or {s7_q5_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q6_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q6_rating",
         title: "You ensure that personal information is accurate, complete and up-to-date as is necessary to fulfil specified purposes.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q6_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q6_risk_assessment",
         visibleIf: "{s7_q6_rating} = \"10\" or {s7_q6_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q7_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q7_rating",
         title: "You provide data subject's with access to their personal information and inform them of its uses and disclosures ",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q7_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q7_risk_assessment",
         visibleIf: "{s7_q7_rating} = \"10\" or {s7_q7_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q8_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q8_rating",
         title: "You provide data subject's with the ability to challenge the accuracy and completeness of the information and have it amended as appropriate ",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q8_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q8_risk_assessment",
         visibleIf: "{s7_q8_rating} = \"10\" or {s7_q8_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q9_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q9_rating",
         title: "You exercise informed privacy decisions for human-machine interfaces to be human-centered, user-centric and user-friendly ",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q9_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q9_risk_assessment",
         visibleIf: "{s7_q9_rating} = \"10\" or {s7_q9_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q10_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q10_rating",
         title: "You demonstrate the data subject featuring prominently at the centre of operations involving collections of personal data",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q10_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q10_risk_assessment",
         visibleIf: "{s7_q10_rating} = \"10\" or {s7_q10_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q11_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q11_rating",
         title: "You have provided the right for data subject's to object direct marketing and profiling",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q11_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q11_risk_assessment",
         visibleIf: "{s7_q11_rating} = \"10\" or {s7_q11_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      },
      {
       type: "panel",
       name: "s7_q12_panel",
       elements: [
        {
         type: "rating",
         name: "s7_q12_rating",
         title: "You have offered data subject's the possibility to export all data from the service.",
         rateValues: [
          {
           value: "10",
           text: "Not achieved"
          },
          {
           value: "5",
           text: "Partially achieved"
          },
          {
           value: "0",
           text: "Achieved"
          }
         ],
         rateMin: "",
         rateMax: 1
        },
        {
         type: "comment",
         name: "s7_q12_comment",
         indent: 3,
         title: "Please justify your answer to the above criterion. In the case of non-applicability, please state your reasons below.",
         rows: 10,
         placeHolder: "Click here to enter."
        },
        {
         type: "matrixdynamic",
         name: "s7_q12_risk_assessment",
         visibleIf: "{s7_q12_rating} = \"10\" or {s7_q12_rating} = \"5\"",
         indent: 3,
         title: "Risk Assessment",
         isRequired: true,
         columns: [
          {
           name: "Risks",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a risk posed by not fully achieving the above criterion.",
           rows: 8
          },
          {
           name: "Controls",
           cellType: "comment",
           isRequired: true,
           placeHolder: "Enter a planned or existing control measure for the risk identified.",
           rows: 8
          },
          {
           name: "Likelihood",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a likelihood level."
          },
          {
           name: "Severity",
           cellType: "rating",
           isRequired: true,
           requiredErrorText: "Please specify a severity level."
          },
          {
           name: "Level of Risk",
           cellType: "expression",
           expression: "({row.Likelihood} * {row.Severity})/25",
           displayStyle: "percent"
          }
         ],
         choices: [
          1,
          2,
          3,
          4,
          5
         ],
         cellType: "comment",
         rowCount: 1,
         minRowCount: 1,
         confirmDelete: true,
         confirmDeleteText: "Are you sure you want to delete this risk?",
         addRowText: "Add Risk",
         removeRowText: "X"
        }
       ]
      }
     ]
    },
    {
     type: "expression",
     name: "s7_risk_measure",
     title: "Risk Measure for Section 7:",
     expression: "(({s7_q1_rating} + {s7_q2_rating} + {s7_q3_rating} + {s7_q4_rating} + {s7_q5_rating} + {s7_q6_rating} + {s7_q7_rating} + {s7_q8_rating} + {s7_q9_rating} + {s7_q10_rating} + {s7_q11_rating} + {s7_q12_rating}) / 120)",
     displayStyle: "percent",
     commentText: "Other (describe)"
    }
   ],
   visibleIf: "{pta_q1} = true or {pta_q2} = true or {pta_q3} = true or {pta_q4} = true or {pta_q5} = true or {pta_q6} = true or {pta_q7} = true or {pta_q8} = true"
  },
  {
   name: "Summary",
   elements: [
    {
     type: "html",
     name: "summary_heading",
     html: "<h3 style=\"text-align: center;\"><span style=\"color: #000000;\">You have completed the PIA.</span></h3>\n<p>&nbsp;</p>\n<p style=\"text-align: center;\"><span style=\"color: #808080;\">Below you'll find a summary of your risk measures from each section, followed by a mean average result for the whole PIA.&nbsp;</span><span style=\"color: #808080;\">This will give you a good indication of what areas of your workflow to focus your resources on.</span></p>\n<p style=\"text-align: center;\"><span style=\"color: #808080;\">Please do not delay on implementing the measures identified under each risk. Action needs to be taken immediately.</span></p>\n<p style=\"text-align: center;\"><span style=\"color: #808080;\">Note: the higher the risk measure the lower the level of compliance and the greater the urgency to resolve issues. </span></p>"
    },
    {
     type: "panel",
     name: "summary_panel",
     elements: [
      {
       type: "matrixdynamic",
       name: "risk_measure_summary",
       title: "Risk Measure Summary by Section",
       columns: [
        {
         name: "Section 1",
         cellType: "expression",
         expression: "{s1_risk_measure}",
         displayStyle: "percent"
        },
        {
         name: "Section 2",
         cellType: "expression",
         expression: "{s2_risk_measure}",
         displayStyle: "percent"
        },
        {
         name: "Section 3",
         cellType: "expression",
         expression: "{s3_risk_measure}",
         displayStyle: "percent"
        },
        {
         name: "Section 4",
         cellType: "expression",
         expression: "{s4_risk_measure}",
         displayStyle: "percent"
        },
        {
         name: "Section 5",
         cellType: "expression",
         expression: "{s5_risk_measure}",
         displayStyle: "percent"
        },
        {
         name: "Section 6",
         cellType: "expression",
         expression: "{s6_risk_measure}",
         displayStyle: "percent"
        },
        {
         name: "Section 7",
         cellType: "expression",
         expression: "{s7_risk_measure}",
         displayStyle: "percent"
        }
       ],
       choices: [
        1,
        2,
        3,
        4,
        5
       ],
       cellType: "expression",
       rowCount: 1,
       addRowText: "Refresh",
       removeRowText: "X"
      },
      {
       type: "expression",
       name: "average_risk_measure",
       title: "Average Risk Measure for PIA:",
       expression: "({s1_risk_measure} + {s2_risk_measure} + {s3_risk_measure} + {s4_risk_measure} + {s5_risk_measure} + {s6_risk_measure} + {s7_risk_measure})/7",
       displayStyle: "percent",
       commentText: "Other (describe)"
      }
     ]
    },
    {
     type: "html",
     name: "summary_footer",
     html: "<h4 style=\"text-align: center;\"><span style=\"color: #000000;\">Please click the 'Complete PIA' button below to download a PDF copy of your responses.</span></h4>\n<p style=\"text-align: center;\"><span style=\"color: #808080;\">PLEASE NOTE: Do not make any changes inside the PDF document. This should serve as a read-only, hard copy. Any changes that you do make will not be reflected in the calculation of your risk measures.&nbsp;</span></p>"
    }
   ],
   visibleIf: "{pta_q1} = true or {pta_q2} = true or {pta_q3} = true or {pta_q4} = true or {pta_q5} = true or {pta_q6} = true or {pta_q7} = true or {pta_q8} = true"
  }
 ],
 showTitle: false,
 showPageTitles: false,
 showQuestionNumbers: "onPage",
 showProgressBar: "top",
 startSurveyText: "Begin PIA",
 pagePrevText: "Previous Section",
 pageNextText: "Next Section",
 completeText: "Complete PIA",
 firstPageIsStarted: true
};

const {user} = useAuth0();
localStorage.setItem('userEmail', user.email)

const sendDataToServer = (survey) => {
  var surveyPDF = new SurveyPDF.SurveyPDF(surveyJSON);
  surveyPDF.data = model.data;
  surveyPDF.save();
 const baseURL = "https://pia-tool.herokuapp.com/beginpia"
  axios.post(baseURL, {
    data: JSON.stringify(survey.data),
    email: user.email
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
}

  var model = new Survey.Model(surveyJSON);
  return (
      <div>
        <Survey.Survey json={ surveyJSON } onComplete={ sendDataToServer } model={ model } />
      </div>
  )

};

export default BeginPIA;
