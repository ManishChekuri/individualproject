// src/components/CompletedPIA.js

import React, {useState, useEffect} from "react";
import axios from "axios";

const CompletedPIA = () => {

  const [survey, setSurvey] = useState([])

  useEffect(() => {
    const fetchItems = () => {
      let localstorage = localStorage.getItem('userEmail')
      let baseURL = `https://pia-tool.herokuapp.com/${localstorage}/surveys`
      axios.get(baseURL)
      .then(res => {
        console.log(res)
        setSurvey(res.data)
      })
      .catch(err => {
        console.log(err)
      })
    }
    fetchItems();
  }, [])


  return (
    <div>
      {survey.map((individualSurvey, index) => {
        return(
          <ul key={index}>
            <li>{JSON.stringify(individualSurvey)}</li>
          </ul>
        )
      })}
    </div>
  );
};

export default CompletedPIA;
