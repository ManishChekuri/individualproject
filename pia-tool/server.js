const {createServer} = require('http');
const compression = require('compression');
const morgan = require('morgan');
const express = require("express");
const path = require("path");
const app = express();
const dev = app.get('env') !== 'production';
const normalizePort = port => parseInt(port, 10);
const PORT = normalizePort(process.env.PORT || 5000);
var bodyParser = require("body-parser");
var cors = require("cors");

if (!dev) {
  app.disable('x-powered-by');
  app.use(bodyParser.urlencoded({extended: true}));
  app.use(bodyParser.json());
  app.use(cors());
  app.use(compression());
  app.use(morgan('common'));
  app.use(express.static(path.resolve(__dirname, 'build')));

let surveys = {};

app.post("/beginpia", (req, res) => {
  if(req.body.email in surveys){
    surveys[req.body.email].push(req.body.data)
    console.log(surveys)
  } else {
      surveys[req.body.email] = []
      surveys[req.body.email].push(req.body.data)
      console.log(surveys)
  }
  res.end()
})

app.get("/:email/surveys", (req, res) => {
  if(surveys[req.params.email] === undefined){
    res.json([])
  } else {
      res.json(surveys[req.params.email])
  }
})

app.get('*', (req,res) => {
  res.sendFile(path.resolve(__dirname, 'build', 'index.html'));
});
}

if(dev) {
  app.use(morgan('dev'));
}

const server = createServer(app);

server.listen(PORT, err => {
  if (err) throw err;

  console.log('Server started!');
});
