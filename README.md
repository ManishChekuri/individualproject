# IndividualProject - Manish Chekuri
## General Data Protection Regulation (GDPR) in Software
This repository contains the code, timelog, meeting minutes and dissertation for my individual project,
which was undertaken during my 4th year at The University of Glasgow.
The project was supervised by Dr Inah Omoronyia, Lecturer in Software Engineering and Information Security (http://www.dcs.gla.ac.uk/~inah/).

-----------------------------------

Privacy by design is a system development philosophy that says that privacy should be taken into account throughout the full system development lifecycle,
from its inception, through implementation and deployment, all the way until the system is decommissioned and no longer used. In software engineering terms
this makes privacy, like security or performance, a software quality attribute or non-functional requirement.

Furthermore, the European Parliament approved in April 2016 the EU General Data Protection Regulation (GDPR). The main goal of such regulation is to protect
the privacy of citizens with regard to the processing of their personal data. It enforces a Privacy by Design and by Default approach, where personal data is
processed only when needed by the functionalities of the information system.

The aim of this project is to therefore investigate how GDPR compliance can be used to verify the extent of privacy by design compliance in software.

-----------------------------------

The Semester 1 status report PDF can be viewed and downloaded here: [SemesterOne PDF](StatusReports/SemesterOne/SemesterOne.pdf).

-----------------------------------

The final dissertation PDF can be viewed and downloaded here: [Dissertation PDF](Dissertation/dissertation.pdf).
All dissertation source materials, including the final pdf, can be found in the [Dissertation](Dissertation) folder.

-----------------------------------

The Privacy Impact Assessment Tool can be accessed via the following link: http://pia-tool.herokuapp.com/.

-----------------------------------
