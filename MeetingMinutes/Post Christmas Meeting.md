# Post Christmas Meeting Minutes - 06/01/2020

First meeting back after the Christmas break. I demonstrated to Inah a working prototype of the first section. He was very happy with the structure and of the tool and also the way the risk measure was calculated. I made use of the SurveyJS library which really helped in the development process and made it a lot easier. I'll be continuing with the implementation and hope to have a full working prototype by the end of the week.

Inah suggested having some kind of back-end to store the results of the survey. A little time needs to be spent looking into this and developing some kind of database.

Evaluation phase will begin from next week and although I already have a few individuals/companies to test the tool out on, more are required. So spend a few hours this week sending e-mails and  calling various organisations.

Also develop a dissertation plan so that the write-up can also begin next week.

We also decided that from now we'll have weekly meetings.

Aims from now till next meeting:
- Finish implementation and deploy to Netlify
- Research into storage method - think SurveyJS has its own storage functionality
- Get more contacts for evaluation
- Design a dissertation plan
