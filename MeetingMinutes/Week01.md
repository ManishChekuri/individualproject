# Week 1 Meeting Minutes - 27/09/2019

Today's introductory meeting, with Inah, was very insightful and allowed me to understand the scope and aims of this particular project. Inah informed me that the overall outcome of the project is to build a Privacy Impact Assessment (PIA) tool based on one or more of the privacy regulations stated below. Such a PIA tool should enable organisations to measure the privacy risk exposure of their processes or product.

The privacy principles that Inah pointed me towards include:
- GDPR regulations (download the GDPR summary from onetrust.com)
- UK data protection act (starting with https://ico.org.uk)
- California Consumer Privacy Act
- Fundamental privacy principles (e.g FIPPs privacy principles and OCED privacy principles)
- Privacy by Design and Default

We agreed that I'll spend the first few weeks of the project researching these principles in detail, so that in the next meeting we would be able to have a flowing conversation on privacy and relevant regulations.

We also established that meetings in the first semester will run on a monthly basis. This is due to the large amount of research required to fully understand the various privacy principles and thus invent a strategy to implement the PIA tool. The regularity of meetings would, however, alter in the second semester. Weekly discussions will be had, maybe even bi-weekly, to ensure that aims are met and schedules are being followed. This seemed to be the best way to organise the workflow of this project, as most of the first semester will be purely spent on research and design activities which will take a significant amount of time before any real progress can be demonstrated to the supervisor. In addition, Inah also advised that it's not worth spending time writing status reports or extensive documentation on my progress - the timelog and these meeting minutes suffice.

We also discussed, very briefly, the workflow for the rest of the year. This is documented below.

Aims for first semester:
- Have an excellent understanding of all relevant privacy principles
- Decide on the technologies to be used for the implementation of the PIA tool
- Be fully proficient in these technologies by undertaking tutorials and reading relevant documentation
- Have a well thought out structure and design for the PIA tool with references to specific articles/recitals from GDPR
- Come up with questions for the PIA tool corresponding to these privacy principles
- Be in a confident position to commence development over the Christmas break

Aims for Christmas break:
- Have a fully, functional first prototype of the PIA tool

Aims for second semester:
- Implement any alterations to the PIA tool
- Evaluation phase
- Dissertation write-up phase

We planned to schedule next meeting over email, after sufficient research has been completed and a good understanding of these privacy principles has been developed.
