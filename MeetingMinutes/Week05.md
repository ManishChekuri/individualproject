# Week 5 Meeting Minutes - 21/10/2019

It's been a month since our last meeting and it was a perfect amount of time to undertake extensive research using the prompts Inah provided in our previous meeting. A lot of reading was conducted during the last few weeks and a plethora of knowledge was developed. We began the meeting with am in-dept conversation about the 7 key privacy principles that GDPR sets out and how best to determine compliance with these. A discussion about privacy, in general, was also conducted and why the development of a PIA tool would enable organisations to operate in a lawful manner. We also spoke, very briefly, about other privacy principles, e.g. foundational privacy principles such as FIPP and OCED.

Following this, I suggested to Inah that I will be creating a Excel sheet to document all these privacy principles and come up with well-worded questions to prove compliance with them. This would be the start of designing the structure for the PIA tool and would allow me to get a greater understanding of what I am trying to develop and how I should go about it.

I am still a bit confused as to what technologies are best to use to implement this tool. When questioning Inah, he replied with having no preference and encouraged me to conduct my own research to identify the best approach. This may involve me having to learn new frameworks and discovering libraries to aid with development.

Aims from now till next meeting:
- Begin designing structure for the PIA tool
- Come up with relevant questions to include in the PIA tool to show compliance with different privacy principles
- Research different technologies to use and identify which would be the best fit
- Conduct further research on GDPR, FIPP, OCED and PbD principles
