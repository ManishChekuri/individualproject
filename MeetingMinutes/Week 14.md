# Week 14 Meeting Minutes - 17/01/2020

The final meeting of first semester involved demonstrating my final design plan for the application. The tool will consist of 7 sections, each section covering a Privacy by Design principle. Each PbD principle was mapped to FIP's and subsequently further questions were added to align with GDPR. This seemed to be a very effective way of structuring the application and Inah was very happy with this strategy.

A lot of Angular tutorials were undertaken since the last meeting, yet I am still unsure whether, with the time we have left, I'll be able to grasp all of its functionality and develop a sustainable tool. Therefore, I suggested to Inah that it might be more wise to switch to a simpler framework, such as React. He had no issues with this.

Aims from now till next meeting:
- Finalise questions: add extras, remove any repeats and reword
- Set up GitLab and get React framework ready
- Look into any libraries that will help with development
