# Week 9 Meeting Minutes - 19/11/2019

The meeting began with a demonstration of all my research conducted, so far, organised into a Excel spreadsheet. A lot of content was present in this file and it clear from Inah's response how overwhelmed he was. He suggested this I should cut down and focus on specifics and construct questions which address mutliple principles (cross-cutting questions). This is a job of cutting-down and portraying my knowledge in a concise way.

I also suggested to Inah the idea of implementing this tool using Ruby on Rails and deploying it on an Apache server. Although he stated that he has no preference he was against the idea of not having a ready-to-go executable which could be deployed on any OS. This then pointed my thoughts to another development strategy that I researched - an Electron Angular Application. This would meet the requirement of developing an executable that could be deployed on any platform - Windows, macOS or Linux. A desktop application will be created that can easily be launched. This would also help in the evaluation stage of this project, where I will be testing this application on organisations.

Furthermore, Inah also suggested having some kind of measure to assess risk - i.e. on a scale from 0 to a 100, where the higher the risk measure the higher the severity/likelihood of the risk. This is something that needs more thought and an efficient methodology needs to be developed.


Aims from now till next meeting:
- Develop concise questions structured into categories that are easy to follow for the user
- Research further on Angular applications and undertake tutorials to make myself familiar with the framework
- Research into risk measure and come up with an effective strategy
- Look further into Privacy by Design and align to other principles, ensuring GDPR compliance
