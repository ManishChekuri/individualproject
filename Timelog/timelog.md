# Timelog

* General Data Protection Regulation (GDPR) in Software
* Manish Chekuri
* 2183255C
* Dr Inah Omoronyia

## Semester 1: Weeks 1 - 13
## Week 1

### Friday 27th September 2019

* *5 hours* Reviewing lecture notes from Tuesday
* *1 hour* Setting up Dissertation template on Overleaf, modified and read through boilerplate
* *1 hour* Setting up GitHub page
* *0.5 hour* Meeting with Inah

### Saturday 28th September 2019

* *5 hours* Researching the 7 Privacy by Design principles
* *2 hours* Organising information from research into Excel spreadsheet
* *1 hour* Briefly researching existing PIA tools already available

## Week 2

## Friday 4th October 2019

* *2 hours* Reviewing Privacy by Design principles and edited spreadsheet
* *4 hours* Researching FIPP/OCED principles
* *2 hours* Aligning FIP's with Privacy by Design Principles

## Saturday 5th October 2019

* *2 hours* Reviewing FIPP/OCED Principles and linked them with Privacy by Design principles - populated spreadsheet
* *3 hours* Reading about California Consumer Privacy Act
* *1 hour* Consolidating points covered in meeting with Inah last week and writing up meeting minutes

## Week 3

## Friday 11th October 2019

* *3 hours* Reading the ICO's guide to GDPR
* *2 hours* Looking into real-life case studies of penalties imposed on organisations as a result of not complying with GDPR - e.g. British Airways and EE
* *2 hours* Researching paper-based PIA tools and their effectiveness

## Saturday 12th October 2019

* *1 hour* Further researching paper-based PIA tools and their difficulty in managing privacy
* *3 hours* Reading OneTrust's PIA Handbook
* *4 hours* Linking GDPR principles with those of FIP and Privacy by Design - populated spreadsheet

## Week 4

## Friday 18th October 2019

* *4 hours* Experimenting with France's CNIL open source PIA software
* *4 hours* Experimenting with Granite's PIA Software - Free Trial for 30 days

## Saturday 19th October 2019

* *3 hours* Organising Excel Spreadsheet by starting with PbD principles and linking them with FIP's and finally aligning to GDPR
* *2 hours* Looking into technologies for implementation - Ruby on Rails
* *3 hours* Watching Ruby Tutorials on YouTube

## Week 5

## Monday 21st October 2019

* *0.5 hour* Meeting with Inah

## Friday 25th October 2019

* *1 hour* Consolidating points covered in meeting with Inah last Monday and writing up meeting minutes
* *2 hours* Splitting up spreadsheet into 7 sheets - 1 sheet for each PbD principle and populating each sheet with links to FIP's and GDPR
* *4 hours* Looking into ICO's guide to pick up further points to incorporate

## Saturday 26th October 2019
* *2 hours* Watching further Ruby Tutorials
* *2 hours* Reading about how to deploy a Ruby application
* *2 hours* Looking into other technologies for implementation

## Week 6

## Friday 1st November 2019

* *4 hours* Coming up with well-worded questions to meet regulations and principles specified in the first 2 sections of spreadsheet
* *1 hour* Amending these questions with additional articles from GDPR, incorporating relevant terminology
* *3 hours* Coming up with questions for the 3rd section and removing any repetitions from the first three sections

## Saturday 2nd November 2019

* *1 hour* Finishing off 3rd section and finding any room for improvements
* *4 hours* Coming up with well-worded questions to meet regulations and principles specified in the fourth and fifth sections of spreadsheet
* *2 hours* Amending these questions and again removing any repeats

## Week 7

## Friday 8th November 2019

* *2 hours* Reviewing questions from Sections 1-5 and making improvements
* *4 hours* Coming up with well-worded questions to meet regulations and principles specified in the sixth and seventh sections of spreadsheet
* *2 hours* Amending these questions and again removing any repeats

## Saturday 9th November 2019

* *2 hours* Coming up with very simple 'Yes/No' questions for the Privacy Threshold Assessment stage to determine whether a PIA is required in the first place
* *4 hours* Reviewing all questions from all sections and making any necessary adjustments
* *1 hour* Making the spreadsheet look nice so that Inah has a clear idea of my vision at the next meeting

## Week 8

## Friday 15th November 2019

* *3 hours* Researching into the Angular framework and weighing its benefits and flaws compared to using Ruby
* *3 hours* Researching into deployment of Angular application via Electron

## Saturday 16th November 2019

* *4 hours* Undertaking Angular tutorials on YouTube
* *3 hours* Looking at alternatives to Angular - React and jQuery

## Week 9

## Tuesday 19th November 2019

* *0.5 hour* Meeting with Inah

## Friday 22nd November 2019

* *1 hour* Consolidating points covered in meeting with Inah last Tuesday and writing up meeting minutes
* *5 hours* Cutting down the questions in each section to there are no overlaps and ensuring each question targets a specific criterion

## Saturday 23rd November 2019

* *3 hours* Reviewing questions and making final adjustments
* *2 hours* Further research into PbD and added additional questions
* *3 hours* Undertaking React tutorials - confirmed choice of framework

## Week 10

## Friday 29th November 2019

* *1 hour* Setting up GitLab and moving over stuff from GitHub page - added Inah as contributor
* *2 hours* Identifying and devising a strategy to measure risk from users response to each question
* *4 hours* Undertaking further React Tutorials and understanding how to use Netlify for deployment - continuous integration

## Saturday 30th November 2019

* *2 hours* Research into libraries to aid with development - SurveyJS
* *1 hour* Setting up development environment and installing packages and tools required for production

## Week 11 - Unable to work on Project this week due to assessment centres for graduate schemes

## Week 12 - PSI Exam this week so unable to work on Project full time

## Thursday 12th December 2019

* *4 hours* Reviewed lecture notes from Tuesday

### Friday 13th December 2019

* *5 hours* Read through Hall of Fame dissertations
* *2 hour* Compiled dissertation template on Gitlab and filled in basic details about project

## Week 13

### Monday 16th December 2019

* *4 hours* Undertook additional research and made further changes to the PIA questions on spreadsheet - all ready now to show Inah the final plan at meeting on Wednesday
* *2 hours* Undertook further React tutorials as well as Express tutorials for the backend server
* *2 hours* Looked into alternative ways to deploy using Heroku as Netlify was undergoing maintenance work over Christmas

## Tuesday 17th December 2019

* *3 hours* Began scouting individuals for user evaluation - sending emails out and contacting various organisations
* *2 hours* Used the create-react-app bootstrap to form a workspace and installed all necessary SurveyJS dependencies
* *2 hours* Experimented with sample surveys to figure out how to embed into a web app
* *1 hour* Began to figure out how to export survey responses into a PDF - still unsure how to successfully do this

### Wednesday 18th December 2019

* *0.5 hour* Meeting with Inah
* *1 hour* Consolidated points covered in meeting with Inah today and wrote up meeting minutes
* *2 hours* Commenced implementation of Privacy Threshold Assessment (Yes/No Questions) using SurveyJS library
* *4 hours* Completed Section 1 of PIA with logic to calculate risk measure for whole section and level of risk for each risk identified
* *1 hour* Minor formatting activities to make first two pages appealing - HTML

## Thursday 19th December 2019

* *2 hours* Writing and submitting status report for deadline tomorrow
* *2 hours* Added instructions above the PTA to assist the user with the use of the tool
* *4 hours* Designed a blank web app with user authentication using Auth0

### Friday 20th December 2019

* *4 hours* Fully completed the development of all 7 PIA sections as well as the initial PTA section and final summary section showing overall risk measure for PIA
* *2 hours* Reviewed each section ensuring correct calculation of risk measures
* *2 hours* Embedded the PIA into the web app - faced a lot of issues with this, still not fully functional (will try to get it working over Christmas)

## Semester 2
## Pre-Term Week

### Monday 6th January 2020

* *4 hours* HOURS FROM CHRISTMAS: Successfully got PIA working in localhost with risk measures calculating correctly
* *0.5 hour* Meeting with Inah
* *1 hour* Consolidated points covered in meeting with Inah today and writing up meeting minutes
* *3 hours* Watched tutorials on how to create an Express server as a backend - implement tomorrow

### Tuesday 7th January 2020

* *2 hours* Sent out further emails for user evaluation - received no responses back from last time, guess it was festive season!
* *2 hours* Implemented a simple ReactJS Navigation Bar
* *1 hour* Designed a very basic "My Profile" page from which the user can Log In from and view their details
* *4 hours* Got an Express server up and running - displaying a JSON.stringify version of Completed PIA's in a new page

### Wednesday 8th January 2020

* *2 hours* Cleaned up code and improved readability
* *2 hours* Used React Router with the Navigation Bar with toggles depending on screen size (use with mobile phones or tablets)
* *2 hours* Replied to emails for user evaluation and sent out more - only one agreed to participate
* *3 hours* Got PDF Export functionality working successfully

### Thursday 9th January 2020

* *2 hours* Ran some walkthroughs to ensure application functions as intended on localhost
* *1 hour* Fixed minor issues on HTML formatting and positioning
* *4 hours* Deployed application on Heroku - faced massive issues with this, express server still not working when deployed

### Friday 10th January 2020

* *2 hours* Successfully deployed PIA onto Heroku and added a 'procfile' to fix the issue of Express server not working when deployed
* *2 hours* Replied to further emails and made calls to get more participants - all declined still only have 1 user willing to participate: Mondelez
* *1 hour* Tried to fix issue of user being able to edit PIA inside PDF (as it'll not update the risk measures) but I need to buy a licence to do this so put a disclaimer in before user exports to PDF telling them not to make any changes inside the PDF
* *3 hours* Began researching how to implement a database into the application - functionality within Heroku to do this but not sure how this would work with SurveyJS

## Week 14

### Friday 17th January 2020

* *0.5 hour* Meeting with Inah
* *3 hours* Tried to set up a MongoDB cluster to save users responses into the database for evaluation - cluster not working and not storing responses
* *3 hours* Experimented with both MySQL and PostgreSQL but faced difficulties with storing responses so opted to improve Export PDF feature and request users to email me their completed PIA for evaluation

### Saturday 18th January 2020

* *1 hours* Consolidating points covered in meeting with Inah yesterday and writing up meeting minutes
* *2 hours* Scouting more companies for evaluation and sent out emails - Designer for French CNLI PIA Tool: Bruno Perles
* *1 hour* Gathered 4th year CS students with some knowledge of privacy to evaluate tool via a walkthrough - set up appointments for tomorrow

### Sunday 19th January 2020

* *5 hours* Undertook walkthroughs with CS students evaluating question understandability and PDF output

## Week 15 - Unable to work on Project this week due to assessment centres for graduate schemes

## Week 16

### Friday 31st January 2020

* *3 hours* Improving layout of PDF export document - bigger text boxes for more clear view of PIA, text was too small previously
* *1 hour* Fixing issues of PDF Export functionality not working on some web browsers
* *2 hours* Scouting more companies for evaluation and sent out emails - 2 companies have confirmed now: Mondelez and Smartsheet

### Saturday 1st February 2020

* *4 hours* Reviewing sections 1-4 and reformatted question titles for it to make more sense based on user evaluation
* *1 hour* Removed 'Download PIA' button and incorporated it into the final button ("Complete PIA") - faced several issues with this affecting the Express server and the JSON string not showing up on the 'Completed PIA' section - all working now

### Sunday 2nd February 2020

* *4 hours* Reviewing sections 5-7 and reformatted question titles for it to make more sense based on user evaluation
* *2 hours* Reviewing PTA and Summary sections - added detailed instructions for working of PIA above the PTA

## Week 17

### Friday 7th February 2020

* *0.5 hour* Meeting with Inah
* *1 hour* Consolidating points covered in meeting with Inah today and writing up meeting minutes
* *2 hours* Preparing instructions to send to companies for evaluation of tool and sent out emails  

### Saturday 8th February 2020

* *4 hours* Looked through Hall of Fame Dissertations again and made notes on point to incorporate in mine
* *2 hours* Started to make a dissertation outline and bullet pointed stuff to include in each section

### Sunday 9th February 2020

* *2 hours* Sent out a few more emails to get more participants - Hughes Walkers Solicitors Edinburgh and Warner Music UK
* *3 hours* Fixed issue that some participants had of not being able to successfully log into the system using Auth0 and facing errors when logging out and logging back in

## Week 18

### Friday 14th February 2020

* *4 hours* Organising all user evaluation responses and analysing evaluation methodology
* *1 hour* Organising and sending out NASA-TLX forms to participants
* *2 hours* Preparing an Excel spreadsheet to enable analysis from NASA-TLX forms

### Saturday 15th February 2020

* *4 hours* Working on the issue of MongoDB not working when the application is deployed

### Sunday 16th February 2020

* *3 hours* Continued trying to fix database issue
* *3 hours* Developed details dissertation plan of points to cover in each section  

## Week 19

### Thursday 20th February 2020

* *0.5 hour* Meeting with Inah

### Friday 21st February 2020

* *1 hour* Consolidating points covered in meeting with Inah yesterday and writing up meeting minutes
* *2 hours* Meeting with David Appleyard from Mondelez International - conducting pilot study
* *3 hours* Organising information collected from David and updating Trello board with achievable objectives

### Saturday 22nd February 2020

* *4 hours* Changing structure of some questions and improving wording - as advised by David
* *3 hours* Working on the issue of MongoDB not working when the application is deployed
* *2 hours* Researching solution to issue - still no luck

### Sunday 23rd February 2020

* *4 hours* Further researching how to successfully deploy app so that MongoDB works on Heroku - added to wishlist on Trello

## Week 20

### Friday 28th February 2020

* *2 hours* Planning Introduction chapter of dissertation
* *4 hours* Planning Background chapter of dissertation and creating relevant figures to include

### Saturday 29th February 2020

* *5 hours* Planning Analysis chapter of dissertation and creating relevant figures to include

### Sunday 1st March 2020

* *3 hours* Planning Design Chapter of dissertation and creating relevant figures to include
* *3 hours* Planning Implementation Chapter of dissertation and creating relevant figures to include

## Week 21

### Friday 6th March 2020

* *4 hours* Sorting out all NASA-TLX forms recieved from evaluation participants and creating tables/graphs
* *2 hours* Organising information from 2 completed PIA's into formats for analysis

### Saturday 7th March 2020

* *5 hours* Analysed completed PIA forms to see if risks and mitigation strategies were accurately identified

### Sunday 8th March 2020

* *3 hours* Creating figures from analysis of completed PIA's to include in Evaluation chapter of dissertation

## Week 22

### Wednesday 11th March 2020

* *0.5 hour* Meeting with Inah
* *1 hour* Consolidating points covered in meeting with Inah today and writing up meeting minutes

### Friday 13th March 2020

* *3 hours* Planning Evaluation chapter of dissertation and creating extra figures to aid in analysis

### Saturday 14th March 2020

* *3 hours* Planning Conclusion section of dissertation and summarised points from participants for any future work

### Sunday 15th March 2020

* *4 hours* Reviewing all sections and adding any further points to include in each one, cutting down any unnecessary information

## Week 23

### Friday 20th March 2020

* *2 hours* Writing up Background section
* *2 hours* Writing up Evaluation section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Saturday 21st March 2020

* *2 hours* Writing up Design section
* *2 hours* Writing up Implementation section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Sunday 22nd March 2020

* *2 hours* Writing up Analysis section
* *2 hours* Writing up Background section
* *1.5 hours* Proofreading today's work and making appropriate changes

## Week 24

### Monday 23rd March 2020

* *2 hours* Writing up Background section
* *2 hours* Writing up Evaluation section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Tuesday 24th March 2020

* *2 hours* Writing up Analysis section
* *2 hours* Writing up Evaluation section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Wednesday 25th March 2020

* *2 hours* Writing up Analysis section
* *2 hours* Writing up Background section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Thursday 26th March 2020

* *2 hours* Writing up Background section
* *2 hours* Writing up Analysis section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Friday 27th March 2020

* *2 hours* Writing up Analysis section
* *2 hours* Writing up Evaluation section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Saturday 28th March 2020

* *2 hours* Writing up Design section
* *2 hours* Writing up Implementation section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Sunday 29th March 2020

* *2 hours* Writing up Evaluation section
* *2 hours* Writing up Background section
* *1.5 hours* Proofreading today's work and making appropriate changes

## Week 25

### Monday 30th March 2020

* *2 hours* Finishing off Background section
* *2 hours* Writing up Evaluation section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Tuesday 31st March 2020

* *2 hours* Finishing off Design section
* *2 hours* Writing up Implementation section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Wednesday 1st April 2020

* *2 hours* Finishing off Implementation section
* *2 hours* Finishing off Analysis section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Thursday 2nd April 2020

* *2 hours* Writing up Evaluation Section
* *2 hours* Finishing off Evaluation section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Friday 3rd April 2020

* *2 hours* Writing up Conclusion section
* *2 hours* Writing up Introduction section
* *1.5 hours* Proofreading today's work and making appropriate changes

### Saturday 4th April 2020

* *2 hours* Finishing off Conclusion section
* *2 hours* Finishing off Introduction section and writing abstract
* *1.5 hours* Proofreading today's work and making appropriate changes

### Sunday 5th April 2020

* *4 hours* Preparing slides and materials (visual aids) for presentation
* *1 hours* Recording presentation video and ensuring correct format
* *3 hours* Proofreading full dissertation

## Week 26

### Monday 6th April 2020

* *0.5 hour* Gathering all assets and following Moodle's guidance for submission

## Total Hours Spent on Project: 457
